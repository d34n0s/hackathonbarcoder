//
//  InventoryMockData.swift
//  BarCoder
//
//  Created by Dean Lawrence on 12/8/19.
//  Copyright © 2019 d34n0s. All rights reserved.
//

import Foundation


class InventoryMockData {
    
    static func createInventoryMockData() -> [InventoryItem] {
        var inventoryList = [InventoryItem]();
        
        let inv1 = InventoryItem(productCode: "orbiWifi00",barCode: "12345",name: "Netgear Orbi Wifi", soh: 5)
        let inv2 = InventoryItem(productCode: "ipadpro2015",barCode: "23456",name: "iPad Pro 2015", soh: 4)
        let inv3 = InventoryItem(productCode: "homepod",barCode: "34567",name: "Apple Home Pod", soh: 3);
        let inv4 = InventoryItem(productCode: "acermonitor27inch",barCode: "45678",name: "Acer LED 27 Inch", soh: 2)
        let inv5 = InventoryItem(productCode: "gforcegamingmouse",barCode: "56789",name: "Logitech G502", soh: 1)
        let inv6 = InventoryItem(productCode: "gigabyteaoruskeyboard",barCode: "67890",name: "Gigabyte Aorus Keyboard", soh: 6)
        let inv7 = InventoryItem(productCode: "Book",barCode: "9781863500296",name: "A Blue Book", soh: 7)
        let inv8 = InventoryItem(productCode: "Another Book",barCode: "9780007145645",name: "A Black Book", soh: 10)
        let inv9 = InventoryItem(productCode: "TISS",barCode: nil,name: "Facial Tissue", soh: 12)
        let inv10 = InventoryItem(productCode: "LOR",barCode: "8711000362594",name: "Coffee Pods", soh: 64)
        
        
        inventoryList.append(inv1)
        inventoryList.append(inv2)
        inventoryList.append(inv3)
        inventoryList.append(inv4)
        inventoryList.append(inv5)
        inventoryList.append(inv6)
        inventoryList.append(inv7)
        inventoryList.append(inv8)
        inventoryList.append(inv9)
        inventoryList.append(inv10)
        
        return inventoryList;
    }
}
