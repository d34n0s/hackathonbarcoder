//
//  InventoryItemModel.swift
//  BarCoder
//
//  Created by Dean Lawrence on 12/8/19.
//  Copyright © 2019 d34n0s. All rights reserved.
//

import Foundation

class InventoryItem {
    
    init(productCode: String,
        barCode: String?,
        name: String,
        soh: Int?) {
        
        self.productCode = productCode
        self.barCode = barCode
        self.name = name
        self.soh = soh
    }
    
    var id: Int?
    var productCode: String
    var barCode: String?
    var name: String
    var soh: Int?
    var uom: String?
    var sku: String?
    
}
