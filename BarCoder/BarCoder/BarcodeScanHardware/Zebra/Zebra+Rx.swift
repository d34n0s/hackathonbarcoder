//
//  Zebra+Rx.swift
//  Synkd
//
//  Created by Dean Lawrence on 14/9/18.
//  Copyright © 2018 Appstablishment. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

//extension ISbtSdkApi : HasDelegate {
//    public typealias Delegate = ISbtSdkApiDelegate
//}

class RxZebraISbtSdkApiDelegateProxy : DelegateProxy<ISbtSdkApi, ISbtSdkApiDelegate>, DelegateProxyType, ISbtSdkApiDelegate {

    static func currentDelegate(for object: ISbtSdkApi) -> ISbtSdkApiDelegate? {
        return object.delegate
    }

    static func setCurrentDelegate(_ delegate: ISbtSdkApiDelegate?, to object: ISbtSdkApi) {
        object.sbtSetDelegate(delegate)
    }

    public weak private(set) var iSbtSdkApi : ISbtSdkApi?

    public init(iSbtSdkApi: ParentObject) {
        self.iSbtSdkApi = iSbtSdkApi
        super.init(parentObject: iSbtSdkApi, delegateProxy: RxZebraISbtSdkApiDelegateProxy.self)
    }

    static func registerKnownImplementations() {
        self.register { RxZebraISbtSdkApiDelegateProxy(iSbtSdkApi: $0) }
    }

}

extension Reactive where Base: ISbtSdkApi {

    public var delegate: DelegateProxy<ISbtSdkApi,ISbtSdkApiDelegate> {
        return RxZebraISbtSdkApiDelegateProxy.proxy(for: base)
    }

    //Connection
    var sbtEventScannerAppeared: Observable<Bool> {
        return delegate.methodInvoked(#selector(ISbtSdkApiDelegate.sbtEventScannerAppeared(_:)))
            .map { _ in
                return true
        }
    }

    var sbtEventScannerDisappeared: Observable<Bool> {
        return delegate.methodInvoked(#selector(ISbtSdkApiDelegate.sbtEventScannerDisappeared(_:)))
            .map { _ in
                return true
        }
    }

    var sbtEventCommunicationSessionEstablished: Observable<Bool> {
        return delegate.methodInvoked(#selector(ISbtSdkApiDelegate.sbtEventCommunicationSessionEstablished(_:)))
            .map { _ in
                return true
        }
    }

    var sbtEventCommunicationSessionTerminated: Observable<Bool> {
        return delegate.methodInvoked(#selector(ISbtSdkApiDelegate.sbtEventCommunicationSessionTerminated(_:)))
            .map { _ in
                return true
        }
    }

    var sbtEventBarcode: Observable<String> {
        return delegate.methodInvoked(#selector(ISbtSdkApiDelegate.sbtEventBarcode(_:barcodeType:fromScanner:)))
            .map { value in
                debugPrint("RxZebraISbtSdkApiDelegateProxy: sbtEventBarcode \(value)")
                let barcode = value.first as! String
                return barcode
        }
    }

//      Delegate Methods from SDK (for reference only)
//    - (void)sbtEventScannerAppeared:(SbtScannerInfo*)availableScanner;
//    - (void)sbtEventScannerDisappeared:(int)scannerID;
//    - (void)sbtEventCommunicationSessionEstablished:(SbtScannerInfo*)activeScanner;
//    - (void)sbtEventCommunicationSessionTerminated:(int)scannerID;
//    - (void)sbtEventBarcode:(NSString*)barcodeData barcodeType:(int)barcodeType fromScanner:(int)scannerID;
//    - (void)sbtEventBarcodeData:(NSData*)barcodeData barcodeType:(int)barcodeType fromScanner:(int)scannerID;

    
}
