//
//  ZebraBarcodeScannerHelper.swift
//  Synkd
//
//  Created by Dean Lawrence on 17/9/18.
//  Copyright © 2018 Appstablishment. All rights reserved.
//

import Foundation
import RxSwift
import RxRelay

class ZebraBarcodeHelper: NSObject, BarcodeProtocol, ISbtSdkApiDelegate {
    
    let bag = DisposeBag()
    
    let barcodeScannerOutput = BarcodeScannerOutput()
    let barcodeScannerInput = BarcodeScannerInput()
    
    var newScannerDetails = BehaviorRelay<(scannerID: Int32, scannerName: String)>(value: (scannerID: 0, scannerName: ""))
    
    let zebra = SbtSdkFactory.createSbtSdkApiInstance()
    
    override init(){
        
        super.init()
        
        setupRx()
        
        zebra?.sbtSetDelegate(self)
        
        //set the type of connections to monitor
        zebra?.sbtSetOperationalMode(Int32(SBT_OPMODE_BTLE))
        
        //set to auto monitor connected scanners
//        The SDK identifies scanners as "available" and "active". An "available" scanner is a scanner that is connected
//        to the iOS device via Bluetooth, but has not yet established a communication session with the SDK. An "active"
//        scanner is a scanner that is both connected to the iOS device via Bluetooth, and has established a
//        communication session with the SDK.
        
        zebra?.sbtSubsribe(forEvents: Int32(SBT_EVENT_SCANNER_APPEARANCE |
            SBT_EVENT_SCANNER_DISAPPEARANCE | SBT_EVENT_SESSION_ESTABLISHMENT |
            SBT_EVENT_SESSION_TERMINATION | SBT_EVENT_BARCODE) )
        
        zebra?.sbtEnableAvailableScannersDetection(true)
        
    }
    
    func getPreviouslyPairedScannerName() {
        
        debugPrint("checking for previous scanner name")
        //get the previous scanner name from user defaults (if not nil), ready to compare to the scanner we just detected to make sure they're the same
        guard let scannerName = UserDefaults.standard.object(forKey: AppInfo.sharedInstance.scannerUserDefaultsKey) as? String
            else { return }
        
        AppInfo.sharedInstance.previouslyPairedScannerName.accept(scannerName)
        
        debugPrint("previous scanner found with name: \(scannerName)")
        
    }

    //MARK: - Generate Pairing Barcode
    class func generatePairingBarcode(factoryReset: Bool, imageFrame: CGRect) -> UIImage? {
        
        let zebraSdk = SbtSdkFactory.createSbtSdkApiInstance()
        
        let resetDefault = factoryReset ? SETDEFAULT_YES : SETDEFAULT_NO
        
        let stcImage = zebraSdk?.sbtGetPairingBarcode(BARCODE_TYPE_STC, withComProtocol: STC_SSI_BLE, withSetDefaultStatus: resetDefault, withBTAddress: "00:00:00:00:00:00", withImageFrame: imageFrame)

        return stcImage
        
    }
    
    func disconnectScanner() {

        UserDefaults.standard.set("None", forKey: AppInfo.sharedInstance.scannerUserDefaultsKey)
        AppInfo.sharedInstance.previouslyPairedScannerName.accept("None")
        
        zebra?.sbtTerminateCommunicationSession(AppInfo.sharedInstance.scannerID)
        
        zebra?.sbtEnableAvailableScannersDetection(true)
        
    }
    
    func subscribeForEvents(_ b: Bool) {
        
        if b {
            zebra?.sbtSubsribe(forEvents: Int32(SBT_EVENT_BARCODE))
        } else {
            zebra?.sbtUnsubsribe(forEvents: Int32(SBT_EVENT_BARCODE))
        }
        
    }
    
    //MARK: - ISbtSdkApiDelegate Methods
    func sbtEventScannerAppeared(_ availableScanner: SbtScannerInfo) {
        
        debugPrint("id: \(availableScanner.getScannerID()) name: \(String(describing: availableScanner.getScannerName()))")
        
        getPreviouslyPairedScannerName()
        
        newScannerDetails.accept((availableScanner.getScannerID(), availableScanner.getScannerName()))
        
    }
    
    func sbtEventScannerDisappeared(_ scannerID: Int32) {
        debugPrint("\(scannerID)")
        
        //reset the scannerID if the scanner that just dissapeared was the one we were connected to
        if scannerID == AppInfo.sharedInstance.scannerID {
            AppInfo.sharedInstance.scannerID = 0
        }
        
        zebra?.sbtEnableAvailableScannersDetection(true)
        
    }

    func sbtEventCommunicationSessionEstablished(_ activeScanner: SbtScannerInfo) {
        
        debugPrint("\(activeScanner.getScannerID())")
        
        barcodeScannerOutput.scannerConnected.accept(activeScanner.isAvailable())
        
        AppInfo.sharedInstance.scannerID = activeScanner.getScannerID()
        
        //Once we have a scanner we can stop trying to detect new ones
        zebra?.sbtEnableAvailableScannersDetection(false)
        
        //try to save the scanner name if we can get it
        guard let scannerName = activeScanner.getScannerName() else { return }
        
        UserDefaults.standard.set(scannerName, forKey: AppInfo.sharedInstance.scannerUserDefaultsKey)
        
        AppInfo.sharedInstance.previouslyPairedScannerName.accept(scannerName)
        
    }
    
    func sbtEventCommunicationSessionTerminated(_ scannerID: Int32) {
        
        if scannerID == AppInfo.sharedInstance.scannerID {
            
            AppInfo.sharedInstance.scannerID = 0
            barcodeScannerOutput.scannerConnected.accept(false)
            
            //Once the comms has been closed we can start looking for scanners again
            zebra?.sbtEnableAvailableScannersDetection(true)
            
        }
        
    }
    
    func sbtEventBarcode(_ barcodeData: String!, barcodeType: Int32, fromScanner scannerID: Int32) {
        
        debugPrint("ZebraBarcodeHelper: sbtEventBarcodeData \(String(describing: barcodeData))")
        
        barcodeScannerOutput.scannerBarcode.accept(String(barcodeData))
        
    }
    
    //MARK: - SetupRx
    
    private func setupRx() {
        
        debugPrint("ZebraBarcodeHelper: SetupRx()")
        
        newScannerDetails.asObservable()
            .filter{
                $0.scannerID != 0 //allow if the scannerID is not 0 and (scanner name is same as previous or previous is empty)
                    && ($0.scannerName == AppInfo.sharedInstance.previouslyPairedScannerName.value || AppInfo.sharedInstance.previouslyPairedScannerName.value == "None") }
            .subscribe(onNext: { [weak self] scanner in
                debugPrint("newScannerDetails scanner found, trying to connect to \(scanner.scannerID) \(scanner.scannerName)")
                self?.zebra?.sbtEstablishCommunicationSession(scanner.scannerID)
            })
        .disposed(by: bag)
        
        barcodeScannerInput.scannerWarningBeep
            .filter { AppInfo.sharedInstance.scannerID != 0}
            .subscribe(onNext: { [weak self] _ in
                
                //BEEP
                let warningBeepXML : String = "<inArgs><scannerID>\(AppInfo.sharedInstance.scannerID)</scannerID><cmdArgs><arg-int>\(SBT_BEEPCODE_FAST_WARBLE)</arg-int></cmdArgs></inArgs>"
                
                debugPrint("ZebraBarcodeHelper: SetupRx() : scannerWarningBeep \(warningBeepXML)")
                
                self?.zebra?.sbtExecuteCommand(Int32(SBT_SET_ACTION), aInXML: warningBeepXML, aOutXML: nil, forScanner: AppInfo.sharedInstance.scannerID)
                
            })
        .disposed(by: bag)
        
        barcodeScannerInput.scannerDisconnect
            .subscribe(onNext: {[weak self] _ in
                self?.disconnectScanner()
            })
        .disposed(by: bag)
        
        barcodeScannerInput.scannerSubscribeForEvents
            .subscribe(onNext: {[weak self] bool in
                self?.subscribeForEvents(bool)
            })
        .disposed(by: bag)
        
    }
    
}
