//
//  BarcodeProtocol.swift
//  Synkd
//
//  Created by Dean Lawrence on 14/9/18.
//  Copyright © 2018 Appstablishment. All rights reserved.
//

import Foundation
import RxSwift
import RxRelay

protocol BarcodeProtocol {
    
    var barcodeScannerOutput: BarcodeScannerOutput { get }
    var barcodeScannerInput: BarcodeScannerInput { get }
}


struct BarcodeScannerOutput {
    
    var scannerConnected = BehaviorRelay<Bool>(value: false)
    var scannerBarcode = BehaviorRelay<String>(value: "")
    
}

struct BarcodeScannerInput {
    
    var scannerNormalBeep = PublishSubject<Void>()
    var scannerWarningBeep = PublishSubject<Void>()
    var scannerDisconnect = PublishSubject<Void>()
    var scannerSubscribeForEvents = PublishSubject<Bool>()
    
}

func getScanHelperIDEnum() -> ScanHelperIDEnum {
    
    return ScanHelperIDEnum(rawValue: UserDefaults.standard.object(forKey: "scanHelperID") as? String ?? "None") ?? .None
    
}
