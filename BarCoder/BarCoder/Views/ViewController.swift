//
//  ViewController.swift
//  BarCoder
//
//  Created by Dean Lawrence on 12/8/19.
//  Copyright © 2019 d34n0s. All rights reserved.
//

import UIKit
import Speech
import RxSwift
import RxCocoa

class ViewController: UIViewController, SFSpeechRecognizerDelegate {

    let viewModel = ViewModel()
    
    let bag = DisposeBag()
    
    //Audio Engine
    let audioEngine = AVAudioEngine()
    let speechRecognizer: SFSpeechRecognizer? = SFSpeechRecognizer()
    let request = SFSpeechAudioBufferRecognitionRequest()
    var recognitionTask: SFSpeechRecognitionTask?
    
    
    @IBOutlet weak var backgroundFlasherView: UIView!
    @IBOutlet weak var barcodeTextField: UITextField!
    @IBOutlet weak var codeTextField: UITextField!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var previousSOHTextField: UITextField!
    @IBOutlet weak var newSOHTextField: UITextField!
    @IBOutlet weak var addButton: UIButton!
    @IBOutlet weak var speechButton: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        enableAddButton(false)
        
        setupRxOutput()
        setupRxInput()
        
    }


    @IBAction func addButtonPressed(_ sender: Any) {
        
        viewModel.input.add.onNext(())
        
    }
    
    @IBAction func speechButtonUp(_ sender: Any) {
        
        print("whale button up")
        
        stopRecordAndRecognizeSpeech()
        
    }
    
    @IBAction func speechButtonUpOutside(_ sender: Any) {
        
        print("whale button up")
        
        stopRecordAndRecognizeSpeech()
        
    }
    
    @IBAction func speechButtonDown(_ sender: Any) {
        
        print("whale button down")
        
        recordAndRecognizeSpeech()
        
    }
    
    func recordAndRecognizeSpeech() {
        
        let node = audioEngine.inputNode
        
        let recordingFormat = node.outputFormat(forBus: 0)
        
        node.installTap(onBus: 0, bufferSize: 1024, format: recordingFormat) { buffer, _ in
            self.request.append(buffer)
        }
        
        audioEngine.prepare()
        
        do {
            try audioEngine.start()
        } catch {
            return print(error)
        }
        
        guard let myRecogniser = SFSpeechRecognizer() else { return }
        
        if !myRecogniser.isAvailable {
            return
        }
        
        recognitionTask = speechRecognizer?.recognitionTask(with: request, resultHandler: { result, error in
        
            if let result = result {
                
                print("speech result: \(result.bestTranscription.formattedString)")
                
                self.viewModel.input.speechInput.accept(result.bestTranscription.formattedString)
                
            } else if let error = error {
                print(error)
            }
        })
    }
    
    func stopRecordAndRecognizeSpeech() {
        
        audioEngine.stop()
        audioEngine.inputNode.removeTap(onBus: 0)
        recognitionTask?.finish()
        
    }
    
    func enableAddButton(_ enabled: Bool) {
        
        print("enableAddButton \(enabled)")
        
        if enabled {
            addButton.isEnabled = true
            addButton.alpha = 1
        } else {
            addButton.isEnabled = false
            addButton.alpha = 0.5
        }
        
    }
    
    func setupRxOutput() {
        
        viewModel.output.barCode.bind(to: barcodeTextField.rx.text).disposed(by: bag)
        viewModel.output.code.bind(to: codeTextField.rx.text).disposed(by: bag)
        viewModel.output.name.bind(to: nameTextField.rx.text).disposed(by: bag)
        
        viewModel.output.barCode.asObservable()
            .subscribe(onNext: {[weak self] barcode in
                
                if barcode.count > 0 {
                    self?.codeTextField.isEnabled = true
                    self?.nameTextField.isEnabled = true
                } else {
                    self?.codeTextField.isEnabled = false
                    self?.nameTextField.isEnabled = false
                }
                
            })
        .disposed(by: bag)
        
        viewModel.output.previousSOH.asObservable()
            .subscribe(onNext: { [weak self] value in
                
                if value == nil {
                    self?.previousSOHTextField.text = ""
                } else {
                    self?.previousSOHTextField.text = String(value ?? 0)
                }
                
            })
        .disposed(by: bag)
        
        viewModel.output.newSOH.asObservable()
            .subscribe(onNext: { [weak self] value in
                
                if value == nil {
                    self?.newSOHTextField.text = ""
                } else {
                    self?.newSOHTextField.text = String(value ?? 0)
                }
                
            })
            .disposed(by: bag)
        
        viewModel.output.itemFound.asObservable()
            .subscribe(onNext: {[weak self] _ in
                self?.flashBackgroundGreen()
            })
        .disposed(by: bag)
        
        viewModel.output.enalbeAddButton.asObservable()
            .subscribe(onNext: {[weak self] bool in
                self?.enableAddButton(bool)
            })
        .disposed(by: bag)
        
        viewModel.output.addButtonTitle.bind(to: addButton.rx.title(for: .normal)).disposed(by: bag)
        
    }
    
    func setupRxInput() {
        
        codeTextField.rx.text.asObservable()
            .subscribe(onNext: {[weak self] text in
                
                self?.viewModel.input.code.accept(text ?? "")
                
            })
        .disposed(by: bag)
        
        nameTextField.rx.text.asObservable()
            .subscribe(onNext: {[weak self] text in
                
                self?.viewModel.input.name.accept(text ?? "")
                
            })
            .disposed(by: bag)
        
        newSOHTextField.rx.text.asObservable()
            .subscribe(onNext: {[weak self] text in
                
                self?.viewModel.input.newSOH.accept(text)
                
            })
            .disposed(by: bag)
        
    }
    
    func flashBackgroundGreen() {
        
//        UIView.animate(withDuration: 1.0, delay: 1.0, options: UIView.AnimationOptions.curveEaseOut, animations: {
//
//            self.backgroundFlasherView.backgroundColor = UIColor.init(red: 0.0, green: 1, blue: 0.0, alpha: 1)
//
//        }, completion: nil)
    }
}

