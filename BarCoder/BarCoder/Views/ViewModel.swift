//
//  ViewModel.swift
//  BarCoder
//
//  Created by Dean Lawrence on 12/8/19.
//  Copyright © 2019 d34n0s. All rights reserved.
//

import Foundation
import RxSwift
import RxRelay

class ViewModel {
    
    let bag = DisposeBag()
    
    let scanHelper = ZebraBarcodeHelper()
    
    let dataRepo = InventoryItemDataRepo()
    
    var output: Output
    var input: Input
    
    var populatingValues: Bool = false
    
    struct Output {
        
        var barCode: BehaviorRelay<String>
        var code: BehaviorRelay<String>
        var name: BehaviorRelay<String>
        var previousSOH: BehaviorRelay<Int?>
        var newSOH: BehaviorRelay<Int?>
        
        var enalbeAddButton: BehaviorRelay<Bool>
        var addButtonTitle: BehaviorRelay<String>
        
        var itemFound: PublishSubject<Void>
        
    }
    
    struct Input {
        
        var barCode: BehaviorRelay<String>
        var code: BehaviorRelay<String>
        var name: BehaviorRelay<String>
        var previousSOH: BehaviorRelay<Int?>
        var newSOH: BehaviorRelay<String?>
        var add: PublishSubject<Void>
        var speechInput: BehaviorRelay<String>
        
    }
    
    init() {
        
        dataRepo.inventoryList = InventoryMockData.createInventoryMockData()
        
        output = Output(
            barCode: BehaviorRelay<String>(value: ""),
            code: BehaviorRelay<String>(value: ""),
            name: BehaviorRelay<String>(value: ""),
            previousSOH: BehaviorRelay<Int?>(value: nil),
            newSOH: BehaviorRelay<Int?>(value: nil),
            enalbeAddButton: BehaviorRelay<Bool>(value: false),
            addButtonTitle: BehaviorRelay<String>(value: "Add"),
            itemFound: PublishSubject<Void>()
        )
        
        input = Input(barCode: BehaviorRelay<String>(value: ""),
                      code: BehaviorRelay<String>(value: ""),
                      name: BehaviorRelay<String>(value: ""),
                      previousSOH: BehaviorRelay<Int?>(value: nil),
                      newSOH: BehaviorRelay<String?>(value: nil),
                      add: PublishSubject<Void>(),
                      speechInput: BehaviorRelay<String>(value: "")
        )
        
        setupRxInput()
        setupRxOutput()
        
    }
    
    
    private func setupRxInput() {
        
        //Search by barcode
        scanHelper.barcodeScannerOutput.scannerBarcode.asObservable()
            .skip(1)
            .subscribe(onNext: {[weak self] barcode in
                print("barcode search: \(barcode)")
                
                if let invItemBarCode = self?.dataRepo.inventoryItem.value?.barCode, barcode == invItemBarCode {
                    
                    //we already have this item loaded, so we'll go to count mode
                    if self?.input.newSOH.value == nil || self?.input.newSOH.value?.count == 0 || self?.input.newSOH.value == "0" {
                        
                        self?.output.newSOH.accept(2)
                        self?.input.newSOH.accept("2")
                        
                    } else {
                        
                        let soh = Int(self?.input.newSOH.value ?? "0") ?? 0
                        
                        self?.input.newSOH.accept(String(soh + 1))
                        self?.output.newSOH.accept(soh + 1)
                        
                    }
                    
                } else {
                    
                    self?.dataRepo.searchByBarcode(barcode)
                    self?.output.barCode.accept(barcode)
                    self?.input.barCode.accept(barcode)
                    
                }
                
                
        })
        .disposed(by: bag)
        
        //Search by productCode
        input.code.asObservable()
            .filter{ return $0.count > 0 }
            .subscribe(onNext: {[weak self] productCode in
                print("productCode search: \(productCode)")
                
                if self?.populatingValues == false {
                    self?.dataRepo.searchByPartCode(productCode)
                }
                
            })
        .disposed(by: bag)
        
        input.add.asObservable()
            .subscribe(onNext: {[weak self] _ in
                
                let newInventoryItem = InventoryItem(
                    productCode: self?.input.code.value ?? "",
                    barCode: self?.input.barCode.value ?? "",
                    name: self?.input.name.value ?? "",
                    soh: Int(self?.input.newSOH.value ?? ""))
                
                self?.dataRepo.saveInventoryItem(newInventoryItem)
            })
            .disposed(by: bag)
        
        let sharedSpeechInput = input.speechInput.asObservable().share()
        
        sharedSpeechInput.asObservable()
            .map { string in
                return Int(string)
            }
            .filter{ $0 != nil }
            .distinctUntilChanged()
            .debounce(RxTimeInterval.milliseconds(500), scheduler: MainScheduler.asyncInstance)
            .subscribe(onNext: { [weak self] int in
                
                if let int = int, self?.output.enalbeAddButton.value == true {
                    
                    print("speech speechInput: \(int)")
                    
                    self?.output.newSOH.accept(int)
                    self?.input.newSOH.accept(String(int))
                    
                }
                
            })
            .disposed(by: bag)
        
        sharedSpeechInput.asObservable()
            .filter{ return $0.lowercased().contains("update") ||
                $0.lowercased().contains("add") ||
                $0.lowercased().contains("save")
            }
            .debounce(RxTimeInterval.seconds(1), scheduler: MainScheduler.asyncInstance)
            .subscribe(onNext: {[weak self] _ in
                
                if self?.output.enalbeAddButton.value == true {
                    self?.input.add.onNext(())
                }
            })
            .disposed(by: bag)
        
        sharedSpeechInput.asObservable()
            .filter{ return $0.lowercased().contains("name")
            }
            .map{ value in
                return value.trimmingCharacters(in: CharacterSet(charactersIn: "Name "))}
            .debounce(RxTimeInterval.seconds(1), scheduler: MainScheduler.asyncInstance)
            .subscribe(onNext: {[weak self] name in
                
                if self?.output.barCode.value.count ?? 0 > 0 {
                    
                    self?.output.name.accept(name)
                    self?.input.name.accept(name)
                
                }
            })
            .disposed(by: bag)
        
        sharedSpeechInput.asObservable()
            .filter{ return $0.lowercased().contains("code")
            }
            .map{ value in
                return value.trimmingCharacters(in: CharacterSet(charactersIn: "Code "))}
            .debounce(RxTimeInterval.seconds(1), scheduler: MainScheduler.asyncInstance)
            .subscribe(onNext: {[weak self] name in
                
                if self?.output.barCode.value.count ?? 0 > 0 {
                    
                    self?.populatingValues = true
                    
                    self?.output.code.accept(name)
                    self?.input.code.accept(name)
                    
                    self?.populatingValues = false
                    
                }
            })
            .disposed(by: bag)
        
        let formInputs = Observable.combineLatest(input.code, input.name)
        
        formInputs.asObservable()
            .subscribe(onNext: {[weak self] _, _ in
                
                let enable = self?.isFormValid() ?? false
                self?.output.enalbeAddButton.accept(enable)
                
            })
        .disposed(by: bag)
        
    }
    
    private func getNumberStringFromSpellOutString(_ spellOut: String) -> String? {
        
        let formatter = NumberFormatter()
        formatter.numberStyle = NumberFormatter.Style.spellOut
        
        let number = formatter.number(from: spellOut.lowercased())
        
        guard let convertedNumber = number else { return nil }
        
        return convertedNumber.stringValue
        
    }
    
    private func setupRxOutput() {
        
        dataRepo.inventoryItem.asObservable()
            .subscribe(onNext: { [weak self] inv in
                
                self?.populatingValues = true
                
                if let i = inv {
                    
                    self?.output.code.accept(i.productCode)
                    self?.input.code.accept(i.productCode)
                    
                    self?.output.name.accept(i.name)
                    self?.input.name.accept(i.name)
                    
                    self?.output.previousSOH.accept(i.soh)
                    self?.input.previousSOH.accept(i.soh)
                    
                    self?.output.enalbeAddButton.accept(true)
                    self?.output.addButtonTitle.accept("Update")
                    
                    self?.output.itemFound.onNext(())
                    
                } else {
                    
                    self?.output.barCode.accept("")
                    
                    self?.output.code.accept("")
                    self?.input.code.accept("")
                    
                    self?.output.name.accept("")
                    self?.input.name.accept("")
                    
                    self?.output.previousSOH.accept(nil)
                    self?.input.previousSOH.accept(nil)
                    
                    self?.output.newSOH.accept(nil)
                    self?.input.newSOH.accept(nil)
                    
                    self?.output.enalbeAddButton.accept(false)
                    self?.output.addButtonTitle.accept("Add")
                }
                
                self?.populatingValues = false
                
            })
        .disposed(by: bag)
        
    }
    
    private func isFormValid() -> Bool {
        
        return input.barCode.value.trimmingCharacters(in: .whitespacesAndNewlines).count > 0
        && input.code.value.trimmingCharacters(in: .whitespacesAndNewlines).count > 0
        && input.name.value.trimmingCharacters(in: .whitespacesAndNewlines).count > 0
        
    }
}
