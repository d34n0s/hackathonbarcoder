//
//  AppInfo.swift
//  BarCoder
//
//  Created by Dean Lawrence on 12/8/19.
//  Copyright © 2019 d34n0s. All rights reserved.
//

import Foundation
import RxSwift
import RxRelay

class AppInfo {

    static let sharedInstance = AppInfo()
    
    var scannerID : Int32 = 0
    var previouslyPairedScannerName = BehaviorRelay<String>(value: "None")
    let scannerUserDefaultsKey = "ZebraScannerName"
    
}
