//
//  InventoryItemDataRepo.swift
//  BarCoder
//
//  Created by Dean Lawrence on 12/8/19.
//  Copyright © 2019 d34n0s. All rights reserved.
//

import Foundation
import RxSwift
import RxRelay

class InventoryItemDataRepo {
    
    //List that holds our InventoryItems
    var inventoryList: [InventoryItem] = []
    
    var inventoryItem = BehaviorRelay<InventoryItem?>(value: nil)
    
    //MARK:- Public Protocol Methods
    func searchByBarcode(_ barcode: String) {
        
        for inventory in inventoryList {
            
            if inventory.barCode == barcode {
                inventoryItem.accept(inventory)
                return
            }
            
        }
        
        inventoryItem.accept(nil)
    }
    
    func searchByPartCode(_ partCode: String) {
        
        for inventory in inventoryList {
            
            if inventory.productCode.lowercased() == partCode {
                inventoryItem.accept(inventory)
                return
            }
        }
        
    }
    
    func saveInventoryItem(_ newInventoryItem: InventoryItem) {
        
        if inventoryItem.value == nil {
            newInventoryItem.id = getNextId()
            inventoryList.append(newInventoryItem)
            inventoryItem.accept(nil)
        } else {
            updateExistingItem(newInventoryItem)
        }
        
    }
    
    private func updateExistingItem(_ newInventoryItem: InventoryItem) {
        
        if let ei = inventoryItem.value { //ei is existing item
            
            ei.barCode = newInventoryItem.barCode
            ei.productCode = newInventoryItem.productCode
            ei.name = newInventoryItem.name
            
            if newInventoryItem.soh != nil {
                ei.soh = newInventoryItem.soh
            }
        }
        
        inventoryItem.accept(nil)
        
    }
 
    //MARK:- Private Methods
    private func getNextId() -> Int {
        
        var id = 1
        
        for inventory in inventoryList {
            
            let inventoryID = inventory.id ?? 0
            
            if  inventoryID > id {
                id = inventoryID + 1
            }
        }
        
        return id
    }
}
